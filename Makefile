RM	=	rm -f

CFLAGS  +=      -Wall -Wextra -Werror -std=c++11

NAME	=	a.out

SRCS	=	Group.cpp \
		Line.cpp \
		main.cpp \
		Parser.cpp \
		Server.cpp


OBJS	=	$(SRCS:.cpp=.o)

CC	=	clang++

all:		$(NAME)

$(NAME):	$(OBJS)
		$(CC) $(OBJS) $(CFLAGS) -o $(NAME)

clean:
		$(RM) $(OBJS)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
