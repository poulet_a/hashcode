#ifndef GROUP_H_
# define GROUP_H_

# include <vector>

class Server;

class Group
{
public:
  Group();
  virtual ~Group();

  void addServer(Server *const);
  double power() const;
  double weight() const;

  std::vector<Server *> _servers;

private:
  Group(const Group &);
  Group &operator=(const Group &);
protected:

};

#endif /* !GROUP_H_ */
