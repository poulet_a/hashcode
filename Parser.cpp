#include "Parser.hpp"

Parser::Parser(std::ifstream &f)
  : file(f), nbLines(0), nbPlaces(0), nbUnavaible(0), nbGroups(0), nbServers(0)
{
}

Parser::~Parser()
{
}

int		Parser::getLine()
{
  std::string	tmp;

  this->iss.str("");
  this->iss.clear();
  if (std::getline(this->file, tmp))
  {
    this->iss.str(tmp);
    return (1);
  }
  return (0);
}

void	Parser::parseFile()
{
  if (getLine())
    this->iss >> nbLines >> nbPlaces >> nbUnavaible >> nbGroups >> nbServers;

  int	x;
  int	y;
  for (int i = 0; i < nbUnavaible; ++i)
  {
    if (getLine())
    {
      iss.clear();
      this->iss >> x >> y;
      coordUnavaible.push_back(std::make_pair(x, y));
    }
  }

  int	place;
  int	capacity;
  for (int i = 0; i < nbServers; ++i)
  {
    if (getLine())
    {
      this->iss >> place >> capacity;
      Servers.push_back(std::make_pair(place, capacity));
    }
  }
}

int	Parser::getNbLines() const
{
  return (this->nbLines);
}

int	Parser::getNbPlaces() const
{
  return (this->nbPlaces);
}

int	Parser::getNbUnavaible() const
{
  return (this->nbUnavaible);
}

int	Parser::getNbGroups() const
{
  return (this->nbGroups);
}

int	Parser::getNbServers() const
{
  return (this->nbServers);
}

Coords	Parser::getCoordUnavaibleN(const int n) const
{
  return (this->coordUnavaible[n]);
}

InfoServer	Parser::getInfoServerN(const int n) const
{
  return (this->Servers[n]);
}

int	Parser::getCapacityTotal() const
{
  int	total;

  total = 0;
  for (int i = 0; i < this->nbServers; ++i)
  {
    total += getInfoServerN(i).second;
  }
  return (total);
}
