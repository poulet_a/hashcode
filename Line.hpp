#ifndef LINE_H_
# define LINE_H_

# include <vector>
# include <iostream>

class Server;

# define EMPTY_PLACE ((Server *)0)
# define LOCKED_PLACE ((Server *)-1)
# define place_iterator std::vector<Server *>::iterator

class Line
{
public:
  Line(int const nbplaces);
  virtual ~Line();
  Line(const Line &);
  Line &operator=(const Line &);

  static bool isUsedPlace(Server *);
  static bool isLockedPlace(Server *);
  static bool isEmptyPlace(Server *);

  place_iterator findFirstEmptyPlace();
  place_iterator findFirstEmptyPlace(size_t const size);
  bool addServer(Server * const);
  bool setLocked(size_t const n);

  int const _nbplaces;
  std::vector<Server *> _places;

protected:

};

std::ostream& operator<<(std::ostream &s, const Line &);

#endif /* !LINE_H_ */
