//
// Parser.hpp for epitech in /home/chapuis_s/rendu/
//
// Made by chapui_s
// Login   <chapui_s@epitech.eu>
//
// Started on  Thu Mar 12 19:57:47 2015 chapui_s
// Last update Thu Mar 12 21:55:58 2015 chapui_s
//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

typedef std::pair<int, int>	Coords;
typedef std::pair<int, int>	InfoServer;

class				Parser
{
private:
  std::ifstream			&file;
  std::istringstream		iss;
  int				nbLines;
  int				nbPlaces;
  int				nbUnavaible;
  int				nbGroups;
  int				nbServers;
  std::vector<Coords>		coordUnavaible;
  std::vector<InfoServer>	Servers;

  int		getLine();

public:
  Parser(std::ifstream &);
  ~Parser();

  void		parseFile();
  int		getNbLines() const;
  int		getNbPlaces() const;
  int		getNbUnavaible() const;
  int		getNbGroups() const;
  int		getNbServers() const;
  Coords	getCoordUnavaibleN(const int n) const;
  Coords	getInfoServerN(const int n) const;
  int		getCapacityTotal() const;
};
