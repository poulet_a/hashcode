#include <iostream>
#include <vector>
#include "Parser.hpp"
#include "Line.hpp"
#include "Group.hpp"
#include "Server.hpp"

int main() {
  std::ifstream f("dc.in");
  Parser parser(f);
  parser.parseFile();
  std::vector<Line *> lines;
  std::vector<Group *> groups;
  std::vector<Server *> servers;
  std::cout << "Parse lines : " << parser.getNbLines() << " of " << parser.getNbPlaces() << std::endl;
  std::cout << "Groups : " << parser.getNbGroups() << " with " << parser.getNbServers() << std::endl;
  std::cout << "Locked : " << parser.getNbUnavaible() << std::endl;
  for (int i = 0; i < parser.getNbLines(); i++)
    lines.push_back(new Line(parser.getNbPlaces()));
  for (int i = 0; i < parser.getNbGroups(); i++)
    groups.push_back(new Group());
  for (int i = 0; i < parser.getNbServers(); i++)
    servers.push_back(new Server(i, parser.getInfoServerN(i).second,
				 parser.getInfoServerN(i).first));
  for (int i = 0; i < parser.getNbUnavaible(); i++)
    lines[parser.getCoordUnavaibleN(i).first]->setLocked(parser.getCoordUnavaibleN(i).second);
  for (int i = 0; i < parser.getNbLines(); i++)
    std::cout << "Line " << i << "\t> " << *lines[i] << std::endl;
  return 0;
}
